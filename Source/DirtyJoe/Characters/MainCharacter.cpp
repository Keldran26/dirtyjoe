// Fill out your copyright notice in the Description page of Project Settings.
#include "MainCharacter.h"
#include "DirtyJoe/Actors/Gun.h"
// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	Gun = GetWorld()->SpawnActor<AGun>(GunClass);
	GetMesh()->HideBoneByName(TEXT("weapon_l"),EPhysBodyOp::PBO_None);
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	Gun->SetOwner(this);
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward",this,&AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight",this,&AMainCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp",this,&AMainCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn",this,&AMainCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUpRate",this,&AMainCharacter::LookUpRate);
	PlayerInputComponent->BindAxis("TurnRate",this,&AMainCharacter::TurnRate);

	PlayerInputComponent->BindAction("Jump",IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump",IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AMainCharacter::Shoot);
}

void AMainCharacter::MoveForward(float Value) 
{
	AddMovementInput(GetActorForwardVector()*Value);
}

void AMainCharacter::MoveRight(float Value) 
{
	AddMovementInput(GetActorRightVector()*Value);
}

void AMainCharacter::LookUp(float Value) 
{
	AddControllerPitchInput(Value);
}

void AMainCharacter::Turn(float Value) 
{
	AddControllerYawInput(Value);
}

void AMainCharacter::LookUpRate(float Value) 
{
	AddControllerPitchInput(Value * LookingRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::TurnRate(float Value) 
{
	AddControllerYawInput(Value * LookingRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::Shoot() 
{
	Gun->PullTrigger();
}
