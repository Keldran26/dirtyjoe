// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DirtyJoeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIRTYJOE_API ADirtyJoeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
